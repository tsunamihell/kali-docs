---
title: Removed Tools From Kali
description:
icon:
weight:
author: ["g0tmi1k",]
---

Unfortunately for various reasons why, we can't always keep every tool in Kali. Below is a list of tools removed from Kali Linux:

| Package | Date | Reason |
|---------|------|--------|
| [bbqsql](https://pkg.kali.org/pkg/bbqsql/) | 2020-03-30 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [clusterd](https://pkg.kali.org/pkg/clusterd/) | 2020-03-30 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [golismero](https://pkg.kali.org/pkg/golismero/) | 2020-03-30 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [killerbee](https://pkg.kali.org/pkg/killerbee/) | 2020-09-03 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [neopi](https://pkg.kali.org/pkg/neopi/) | 2020-04-09 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [peepdf](https://pkg.kali.org/pkg/peepdf/) | 2020-04-07 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [pyrit](https://pkg.kali.org/pkg/pyrit/) | 2020-04-02 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [rfidiot](https://pkg.kali.org/pkg/rfidiot/) | 2020-03-30 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [sandsifter](https://pkg.kali.org/pkg/sandsifter/) | 2020-07-20 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [volafox](https://pkg.kali.org/pkg/volafox/) | 2020-04-09 | Python 2 (#1, #2, [#3](https://www.kali.org/blog/python-2-end-of-life/))
| [volatility](https://pkg.kali.org/pkg/volatility) / [volatility3](https://pkg.kali.org/pkg/volatility3) | 2020-10-06 | License change ([#1](https://github.com/volatilityfoundation/volatility3/issues/208), [#2](https://lists.fedoraproject.org/archives/list/legal@lists.fedoraproject.org/thread/OHECHDPLDJ7LLFUZXQMBBAXEXYTQMXOR/)) |
| [webhandler](https://pkg.kali.org/pkg/webhandler) | 2020-03-30 | Python 2 ([#1](https://github.com/lnxg33k/webhandler/issues/21), [#2](https://gitlab.com/kalilinux/packages/webhandler/-/issues/1), [#3](https://www.kali.org/blog/python-2-end-of-life/)) |
